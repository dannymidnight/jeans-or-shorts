require(
  [
    'weather',
    'lib/moustache'
  ],
  function(weather, Moustache) {
    var JOS = {
      PLAY_SONG: true,
      
      template: "It's {{{ temp }}} and {{{ text }}}, with a high of {{{ high }}} and low of {{{ low }}}.",
      
      shorts: [
        { image: 'bikes.jpg', video: 'UcvjXAtzaMU' },		// The Royal Teans - Short Shorts
        { image: 'dog.jpg', video: 'UcvjXAtzaMU' },
        { image: 'kids.jpg', video: 'UcvjXAtzaMU' },
        { image: 'yellow.jpg', video: 'UcvjXAtzaMU' }
      ],

      jeans: [
        { image: 'jumper.jpg', video: 'UWdcZqG02Ls' },		// David Dundas - Jeans On
        { image: 'moustache.jpg', video: 'UWdcZqG02Ls' },
        { image: 'beard.jpg', video: 'UWdcZqG02Ls' }
        // { image: 'ladies.jpg', video: 'UWdcZqG02Ls' },
        // { image: 'smoke.jpg', video: 'UWdcZqG02Ls' },
        // { image: 'skates.jpg', video: 'UWdcZqG02Ls' }
      ],

      addVideo: function(id) {
        if (JOS.PLAY_SONG) {
          $('#container').append(
            '<embed id="pants_video" src="http://www.youtube.com/v/'+id+'&autoplay=1" type="application/x-shockwave-flash" wmode="transparent" width="425" height="344"></embed>'
          );
        }
      },

      renderError: function(msg) {
        $("#info").empty()
          .append('<span>' + msg + '</span>')
          .append('<span>Please <a href="#" id="try-again">try again</a>.</span>');

        $('#try-again').click(function(e) {
          e.preventDefault();
          JOS.start();
        });
      },

      renderContent: function(data) {
        var weather = data.weather;
        var template = JOS.template;

        weather.symbol = '<span class="celcius">°C</span>';
        weather.high = '<span class="high">' + weather.high + weather.symbol + '</span>';
        weather.low = '<span class="low">' + weather.low + weather.symbol + '</span>';
        weather.temp = '<span class="temp">' + weather.temp + weather.symbol + '</span>';
        weather.text = '<span id="condition" class="condition">' + weather.text.toLowerCase() + '</span>';

        $("#why").html(Mustache.render(template, weather));
      },

      renderPants: function(data) {
        JOS.showPants(data.pants);

        var image = weather.images[data.weather.code];
        if (image) {
          $("#condition").append(
            $('<img object id="forecast" />')
              .attr('src', 'images/weather/' + image + '.png')
              .attr('title', data.weather.text)
          );
        }

        JOS.renderContent(data);

        $("#header").hide();
        $("#info").hide();
        $("#pants_holder").show();
        $("#social").show();
      },

      showPants: function(pants) {
        var item = JOS[pants][Math.floor(Math.random() * JOS[pants].length)];

        $('#pants').attr('src', 'images/' + pants + '/' + item.image);
        JOS.addVideo(item.video);

        $('#pants-header')
          .addClass(pants)
          .text(pants);
      },

      getPants: function(location) {
        $.get('/pants', location, function(data) {
          if (data.status === 'ok') {
            // Pants.
            JOS.renderPants(data);
          } else {
            // No pants :(
            JOS.renderError('Problem occured whilst fetching pants.');
          }
        });
      },

      start: function() {
        $("#header").show();
        $("#info").html('<span>Loading pants...</span>').show();
        JOS.getPants({
          location: geoip_city()
        });
      }
    };

    JOS.start();
  }
);
