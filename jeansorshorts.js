var express = require('express'),
    weather = require('weather'),
    gzippo  = require('gzippo'),
    config  = require('./config');

var THRESHOLD = 23;

var logger = {
  log: function() {
    console.log.apply(console, arguments);
  }
};

var app = express.createServer()
  .use(express.static(__dirname + '/public'))
  .use(express.bodyParser())
  .set('views', __dirname + '/views')
  .set('view options', { layout: false })
  .set('view engine', 'jade');

app.get('/', function(req, res) {
  res.render('index', {
    env: app.settings.env
  });
});

app.get('/pants', function(req, res) {
  var args = req.query,
      location = args.location;

  logger.log('Fetching weather for: %s ', JSON.stringify(location));

  // Define error response.
  var errorResponse = function(msg) {
    logger.log(msg);
    res.send({
      status: 'error'
    });
  };
  weather.error = errorResponse;

  var options = {
    logging: config.logging,
    location: location,
    appid: config.appid
  };

  try {
    weather(options, function(weather) {
      // Complex maths.
      var pants = weather.temp < THRESHOLD ? 'jeans' : 'shorts';

      // Send it.
      res.send({
        weather: weather,
        pants:  pants,
        status: 'ok'
      });
    });
  }
  catch (e) {
    errorResponse(e);
  }
});

var port = process.env.PORT || 3000;
app.listen(port, function() {
  logger.log("ENV: %s", app.settings.env);
  logger.log("Listening on port %d", app.address().port);
});
