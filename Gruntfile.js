module.exports = function(grunt) {
  grunt.initConfig({
    requirejs: {
      compile: {
        options: {
          almond: true,
          appDir: './public/js',
          baseUrl: './',
          mainConfigFile: 'public/js/main.js',
          dir: 'public/build',
          modules: [{
            name: 'jeansorshorts'
          }]
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-requirejs');
  grunt.registerTask('default', ['requirejs']);
};
